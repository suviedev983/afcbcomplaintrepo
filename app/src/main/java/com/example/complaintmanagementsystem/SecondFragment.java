package com.example.complaintmanagementsystem;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Debug;
import android.text.Editable;
import android.util.ArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;



import androidx.annotation.NonNull;

import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.complaintmanagementsystem.databinding.FragmentSecondBinding;
import com.example.complaintmanagementsystem.ui.login.LoginActivity;
import com.parse.ParseObject;
import com.parse.ParseUser;


import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SecondFragment extends Fragment {

    private FragmentSecondBinding binding;
    private Spinner spinnerList;
    ParseUser loggegInUser;

    private ArrayAdapter<CharSequence> adapter;
    private ArrayAdapter<CharSequence> adapter2;
    private EditText complainText, qNo;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        super.onCreate(savedInstanceState);

        loggegInUser = ParseUser.getCurrentUser();

        if(loggegInUser  == null){
            Toast.makeText(getContext(), "NOT LOGGED IN", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivity(intent);
        }

        binding = FragmentSecondBinding.inflate(inflater, container, false);


        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //LinearLayout layout = (LinearLayout) getView();
        //String[] objects = { "B/R", "E/M", "Furniture", "Others" };
        adapter=ArrayAdapter.createFromResource(this.getActivity(), R.array.natureOfComplaintList, android.R.layout.simple_spinner_item);

        spinnerList= (Spinner) binding.getRoot().findViewById(R.id.natureOfPicklist);

        complainText  = (EditText)binding.getRoot().findViewById(R.id.complaintText);
        qNo = (EditText)binding.getRoot().findViewById(R.id.QNo);

        // adapter2=ArrayAdapter.createFromResource(this.getActivity(),qNos , android.R.layout.simple_spinner_item);

        // adapter = new ArrayAdapter(
        //  getActivity(),android.R.layout.simple_list_item_1 ,objects);
            if (spinnerList != null) {
                spinnerList.setAdapter(adapter);

            }
            //ENTER HERE


        binding.submitComplaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(loggegInUser != null) {
                    ComplaintEntity entObj = new ComplaintEntity();


                    Object obNature = spinnerList.getSelectedItem();
                    Editable compl, qNoEdit;
                    compl = complainText.getText();
                    qNoEdit = qNo.getText();

                    String natureStr, complainT, qNum;
                    natureStr = "";
                    complainT = "";
                    qNum = "";
                    Boolean val = true;
                    if (obNature != null) {
                        natureStr = obNature.toString();
                    } else {
                        val = false;
                        Toast.makeText(getActivity(), "obNature is nULL", Toast.LENGTH_LONG).show();
                    }
                    if (compl != null) {
                        complainT = compl.toString();
                    } else {
                        val = false;
                        Toast.makeText(getActivity(), "complainT is nULL", Toast.LENGTH_LONG).show();
                    }
                    if (qNoEdit != null) {
                        qNum = qNoEdit.toString();
                    } else {
                        val = false;
                        Toast.makeText(getActivity(), "qNum is nULL", Toast.LENGTH_LONG).show();
                    }
                    if (val) {

                        if (natureStr.isEmpty() || complainT.isEmpty() || qNum.isEmpty()) {
                            if (natureStr.isEmpty() || natureStr.equalsIgnoreCase("None")) {
                                Toast.makeText(getActivity(), "Please Enter Nature of Complaint", Toast.LENGTH_LONG).show();
                            }
                            if (complainT.isEmpty()) {
                                Toast.makeText(getActivity(), "Please Enter Complaint Text", Toast.LENGTH_LONG).show();
                            }
                            if (qNum.isEmpty()) {
                                Toast.makeText(getActivity(), "Please Enter Quarter Num", Toast.LENGTH_LONG).show();
                            }
                            //Toast.makeText(getActivity(), "Please Enter all the fields", Toast.LENGTH_LONG).show();
                        } else {
                            entObj.setStatusText("NEW");
                            entObj.setNatureOfComplaint(natureStr);
                            entObj.setComplaintText(complainT);
                            entObj.setQuarterNo(qNum);
                            String retStr = BusinessTierImpl.postComplaint(entObj);
                            AlertDialog.Builder dialogBoxEntity = new AlertDialog.Builder(getContext());
                            dialogBoxEntity.setTitle("Successfully Saved!");
                            dialogBoxEntity.setMessage(retStr);

                            dialogBoxEntity.setView(getView());

                            dialogBoxEntity.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            AlertDialog helpDialog = dialogBoxEntity.create();

                            if (helpDialog != null) {
                                //helpDialog.show();
                            }

                            Toast.makeText(getActivity(), retStr, Toast.LENGTH_LONG).show();
                            complainText.setText("");
                            qNo.setText("");


                        }
                    }
                }
                else{
                    Toast.makeText(getContext(), "NOT LOGGED IN", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intent);
                }
            }


        });






    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}