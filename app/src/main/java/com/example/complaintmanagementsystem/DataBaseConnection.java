package com.example.complaintmanagementsystem;

import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.parse.Parse;
import com.parse.ParseObject;
import com.parse.ParseUser;

public class DataBaseConnection {

    public static String postComplaintPersist(ComplaintEntity entObj){

        Connection conn1 = null;
        String result = " ";
        String returnStr = "";

        try {

        ParseObject firstObject = new  ParseObject("Complaint");
        firstObject.put("ComplaintText",entObj.getComplaintText());
        firstObject.put("NatureOfIssue",entObj.getNatureOfComplaint());
        firstObject.put("Status",entObj.getStatusText());
        firstObject.put("QNo",entObj.getQuarterNo());

        firstObject.save();
            returnStr = "Successfully Saved";
        }
        catch(Exception e){
            returnStr = e.getMessage();
        }


        return returnStr;

    }

}
