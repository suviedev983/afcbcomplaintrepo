package com.example.complaintmanagementsystem;

public class ComplaintEntity {

    private String complaintText;
    private String natureOfComplaint;
    private String quarterNo;
    private String statusText;

    public String getComplaintText() {
        return complaintText;
    }

    public void setComplaintText(String complaintText) {
        this.complaintText = complaintText;
    }

    public String getNatureOfComplaint() {
        return natureOfComplaint;
    }

    public void setNatureOfComplaint(String natureOfComplaint) {
        this.natureOfComplaint = natureOfComplaint;
    }

    public String getQuarterNo() {
        return quarterNo;
    }

    public void setQuarterNo(String quarterNo) {
        this.quarterNo = quarterNo;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }
}
