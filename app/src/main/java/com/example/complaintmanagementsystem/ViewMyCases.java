package com.example.complaintmanagementsystem;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import com.example.complaintmanagementsystem.databinding.FragmentSecondBinding;
import com.example.complaintmanagementsystem.databinding.FragmentViewMyCasesBinding;

/**
 * A simple {@link Fragment} subclass.

 * create an instance of this fragment.
 */
public class ViewMyCases extends Fragment {

    // TODO: Rename parameter arguments, choose names that match


    private List<ComplaintEntity> entityList;
    private String uId;
    private FragmentViewMyCasesBinding binding;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_view_my_cases, container, false);
        binding = FragmentViewMyCasesBinding.inflate(inflater, container, false);

        entityList = BusinessTierImpl.getComplaints(uId);
        // View v = inflater.inflate(R.layout.fragment_view_my_cases,container,false);
        //TableLayout mytable = new TableLayout(getActivity());
        TableLayout mytable = (TableLayout) v.findViewById(R.id.displayTable);
        //layout.getLayoutParams();
        /*if(layout != null){
            Toast.makeText(getActivity(), "FOUND TABLE", Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(getActivity(), "TABLE NOW FOUND", Toast.LENGTH_LONG).show();
        }*/
        //LinearLayout linearLayout = (LinearLayout) v.findViewById(R.id.tLayout);
        for(ComplaintEntity entityObj : entityList){

            Toast.makeText(getActivity(), entityObj.getComplaintText(), Toast.LENGTH_LONG).show();
            TableRow newRow = new TableRow(getActivity());
            newRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView complain = new TextView(getActivity());
            TextView qNo = new TextView(getActivity());
            TextView status = new TextView(getActivity());
            complain.setText(entityObj.getComplaintText());
            TableLayout.LayoutParams layoutParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);
            complain.setTextColor(Color.BLACK);
            complain.setGravity(Gravity.CENTER_HORIZONTAL);
            qNo.setGravity(Gravity.CENTER_HORIZONTAL);
            status.setGravity(Gravity.CENTER_HORIZONTAL);
            qNo.setText(entityObj.getQuarterNo());
            qNo.setTextColor(Color.BLACK);
            status.setText(entityObj.getStatusText());
            status.setTextColor(Color.BLACK);
           // complain.setLayoutParams(layoutParams);
           // qNo.setLayoutParams(layoutParams);
            //status.setLayoutParams(layoutParams);
            newRow.addView(complain);

            newRow.addView(qNo);
            newRow.addView(status);
            View table_row_view = v.inflate(getActivity(),R.layout.fragment_view_my_cases,newRow);
            newRow.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            //linearLayout.removeAllViews();
            try {
               // linearLayout.addView(complain);
               // linearLayout.addView(qNo);
               // linearLayout.addView(status);
                //layout.addView(newRow,new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
               // mytable.addView(newRow, new TableLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                mytable.addView(newRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));

            }
            catch(Exception e){
                Toast.makeText(getActivity(), "Error Occurred", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

            ///layout.removeAllViews();
            //Toast.makeText(getActivity(), "Working", Toast.LENGTH_LONG).show();
           // layout.addView(newRow,new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));


        }

       // RecyclerView recycler = (RecyclerView) binding.getRoot().findViewById(R.id.recyclerView);



        //getActivity().setContentView(mytable);
        return v;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}