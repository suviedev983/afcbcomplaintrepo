package com.example.complaintmanagementsystem.data;

import com.parse.ParseUser;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    public Result<ParseUser> login(String username, String password) {

        ParseUser loggedInUser = null;
        AtomicReference<String> errStr = new AtomicReference<String>();
        Result<ParseUser> retVal = null;

       /*try {
            // TODO: handle loggedInUser authentication
            /*LoggedInUser fakeUser =
                    new LoggedInUser(
                            java.util.UUID.randomUUID().toString(),
                            "Jane Doe");
            return new Result.Success<>(fakeUser);
            return null;
        } catch (Exception e) {
            return new Result.Error(new IOException("Error logging in", e));
        }*/
        try {
            ParseUser.logIn(username, password);
        /*ParseUser.logInInBackground(username, password, (parseUser, e) -> {
            //progressDialog.dismiss();
            if (parseUser != null) {

                //showAlert("Successful Login", "Welcome back " + username + " !");
            } else {
                if(e != null) {
                    errStr.set(new String(e.getMessage()));
                }
                else{
                    errStr.set(new String("NULL E"));
                }
                ParseUser.logOut();


            }
        });*/

            loggedInUser = ParseUser.getCurrentUser();


            if (loggedInUser != null) {
                retVal = new Result.Success<>(loggedInUser);
            } else {
                retVal = new Result.Error(new IOException("NULL USER"));
            }

        }
        catch(Exception e){
            retVal = new Result.Error(new Exception(e.getMessage()));
        }
        return retVal;
    }

    public void logout() {
        // TODO: revoke authentication
    }
}