package com.example.complaintmanagementsystem.ui.login;

import android.os.Bundle;

import com.example.complaintmanagementsystem.databinding.ActivityResistrationBinding;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.complaintmanagementsystem.ui.login.*;

import com.example.complaintmanagementsystem.R;

public class Registration extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;
    private ActivityResistrationBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityResistrationBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        EditText userName,password,email;
        userName = binding.userName;
        password = binding.editTextTextPassword;
        email = binding.emailId;
        Button registerButton;
        registerButton = binding.register;
        //setSupportActionBar(binding.toolbar);


       /* binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Editable userNameTXT,emailTXT,passwordTXT;
                String userNameStr,emailStr,passwordStr;
                userNameStr = "";
                emailStr = "";
                passwordStr = "";
                userNameTXT = userName.getText();
                passwordTXT = password.getText();
                emailTXT = email.getText();
                Boolean val = true;

                if(userNameTXT != null){
                    userNameStr = userNameTXT.toString();
                }
                else{
                    val=false;
                    Toast.makeText(getApplicationContext(), "userName is nULL", Toast.LENGTH_LONG).show();
                }
                if(passwordTXT != null){
                    passwordStr = passwordTXT.toString();
                }
                else{
                    val=false;
                    Toast.makeText(getApplicationContext(), "Password is nULL", Toast.LENGTH_LONG).show();
                }

                if(emailTXT != null){
                    emailStr = emailTXT.toString();
                }
                else{
                    val=false;
                    Toast.makeText(getApplicationContext(), "Email is nULL", Toast.LENGTH_LONG).show();
                }
                if(val) {
                    if (emailStr.isEmpty() || passwordStr.isEmpty() || userNameStr.isEmpty()) {
                        if (emailStr.isEmpty()) {
                            Toast.makeText(getApplicationContext(), "Please Enter Email", Toast.LENGTH_LONG).show();
                        }
                        if (passwordStr.isEmpty()) {
                            Toast.makeText(getApplicationContext(), "Please Enter Password", Toast.LENGTH_LONG).show();
                        }
                        if (userNameStr.isEmpty()) {
                            Toast.makeText(getApplicationContext(), "Please Enter User Name", Toast.LENGTH_LONG).show();
                        }

                        //Toast.makeText(getApplicationContext(), "Please Enter all the fields", Toast.LENGTH_LONG).show();
                    } else {
                        if(passwordStr.length() < 8){
                            Toast.makeText(getApplicationContext(), "Please Enter Password of length greater than 8 letters", Toast.LENGTH_LONG).show();
                        }
                        else {
                            UserEntity us = new UserEntity();
                            us.setEmailId(emailStr);
                            us.setPassword(passwordStr);
                            us.setUserName(userNameStr);
                            us.setEmail(false);
                            us.setAdmin(false);

                            String retStrSave = RegisterUser.register(us);

                            Toast.makeText(getApplicationContext(), retStrSave, Toast.LENGTH_LONG).show();
                            userName.setText("");
                            password.setText("");
                            email.setText("");
                        }
                    }
                }

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
                return true;
    }
}