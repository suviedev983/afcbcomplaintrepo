package com.example.complaintmanagementsystem.ui.login;

import com.parse.ParseObject;
import com.parse.ParseUser;

public class RegisterUser {

    public static String register(UserEntity entObj){

        String ret = null;

        try{

            ParseUser user = new ParseUser();
            user.setUsername( entObj.getUserName());
            user.setPassword( entObj.getPassword());
            user.setEmail(entObj.getEmailId());
            user.signUp();

           // firstObject.put("isAdmin",entObj.isAdmin);
           // firstObject.put("emailVerified",entObj.isEmail);

            //firstObject.save();
            ret = "Successfully Saved";
        }
        catch(Exception e){
            ret = e.getMessage();
        }

        return ret;


    }

}
