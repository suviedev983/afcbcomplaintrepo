package com.example.complaintmanagementsystem;

import java.util.ArrayList;
import java.util.List;

public class BusinessTierImpl {

    public static String postComplaint(ComplaintEntity entObj){

        String returnStr = null;

        returnStr = DataBaseConnection.postComplaintPersist(entObj);

        return returnStr;

    }

    public static List<ComplaintEntity> getComplaints(String id){

        List<ComplaintEntity> listComplaintEntity = new ArrayList<ComplaintEntity>();

        ComplaintEntity entity = new ComplaintEntity();
        entity.setComplaintText("Dummy Return Text");
        entity.setNatureOfComplaint("E/R");
        entity.setQuarterNo("Q102");
        entity.setStatusText("Pending");
        ComplaintEntity entity2 = new ComplaintEntity();
        entity2.setComplaintText("Dummy Return Text2");
        entity2.setNatureOfComplaint("E/R");
        entity2.setQuarterNo("Q103");
        entity2.setStatusText("New");
        listComplaintEntity.add(entity);
        listComplaintEntity.add(entity2);
        return listComplaintEntity;

    }
}
